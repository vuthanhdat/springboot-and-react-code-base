package com.saigonesetech.springreact.web.interceptor;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

public class GlobalModelInterceptor extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		WebUserSession webUserSession = new WebUserSession(SecurityContextHolder.getContext().getAuthentication(),
				LocaleContextHolder.getLocale());
		
		UserSessionHolder.set(webUserSession);
		
		return super.preHandle(request, response, handler);
	}
	
	@Override
	public void postHandle(HttpServletRequest req, HttpServletResponse res, Object handler, ModelAndView mnv) throws Exception {
		if (Objects.isNull(mnv)) {
			return;
		}
		
		mnv.getModelMap().addAttribute("auth", ((WebUserSession) UserSessionHolder.get()).getAuthInfoMap());
	}
}

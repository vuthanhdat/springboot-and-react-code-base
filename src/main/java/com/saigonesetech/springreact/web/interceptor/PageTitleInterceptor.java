package com.saigonesetech.springreact.web.interceptor;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PageTitleInterceptor extends HandlerInterceptorAdapter {
	private String pageTitle;
	
	public PageTitleInterceptor(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	
	@Override
	public void postHandle(HttpServletRequest req, HttpServletResponse res, Object handler, ModelAndView mnv) throws Exception {
		if (mnv == null) {
			return;
		}
		
		//if the view is redirect we bypass set pageTitle and leave it for redirected flow.
		if (mnv.getViewName().startsWith("redirect:/") || mnv.getView() instanceof RedirectView) {
			return;
		}
		
		mnv.getModelMap().addAttribute("pageTitle", pageTitle);
	}
}

package com.saigonesetech.springreact.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Objects;

import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableConfigurationProperties({SecurityConfig.Admins.class, SecurityConfig.Guests.class})
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	private static final String XSRF_TOKEN_NAME = "XSRF-TOKEN";
	
	private final ObjectMapper mapper;
	private final BuiltInUserDetailService builtInUserDetailService;
	private final Admins admins;
	private final Guests guests;
	
	@Autowired
	public SecurityConfig(ObjectMapper mapper, Admins admins, Guests guests, BuiltInUserDetailService builtInUserDetailService) {
		this.mapper = mapper;
		this.admins = admins;
		this.guests = guests;
		this.builtInUserDetailService = builtInUserDetailService;
	}
	
	private static CsrfTokenRepository csrfTokenRepository() {
		HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
		
		repository.setHeaderName("X-XSRF-TOKEN");
		
		return repository;
	}
	
	private static void setSecurityTokens(HttpServletRequest request, HttpServletResponse response) {
		CsrfToken csrf = (CsrfToken) request.getAttribute("_csrf");
		
		if (Objects.isNull(csrf)) {
			return;
		}
		
		Cookie cookie = WebUtils.getCookie(request, XSRF_TOKEN_NAME);
		String token = csrf.getToken();
		
		if (Objects.isNull(cookie) || Objects.nonNull(token) && !token.equals(cookie.getValue())) {
			cookie = new Cookie(XSRF_TOKEN_NAME, token);
			cookie.setPath("/");
			cookie.setHttpOnly(false);
			response.addCookie(cookie);
		}
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) {
		try {
			if (builtInUserDetailService != null) {
				auth.userDetailsService(builtInUserDetailService);
			}
		} catch (Exception e) {
			throw new CDNException("Could not initialize In-memory authentication", e);
		}
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/js/**", "/assets/**", "/favicon.ico");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.authorizeRequests()
				.antMatchers("/assets/**", "/js/**").permitAll()
				.anyRequest().authenticated()
				.and()
				.formLogin()
				.loginPage(SIGNIN_URL)
				.loginProcessingUrl("/api/authenticate")
				.successHandler(this::handleAjaxAuthSuccess)
				.failureHandler(this::handleAjaxAuthFailure)
				.permitAll()
				.and()
				.rememberMe()
				.rememberMeCookieName("JREMEMBERME")
				.tokenValiditySeconds(90 * 24 * 60 * 60) // 90 days
				.and()
				.addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
				.csrf().csrfTokenRepository(csrfTokenRepository())
				.and()
				.logout()
				.logoutUrl(SIGNOUT_URL)
				.logoutSuccessHandler((request, response, authen) -> response.sendRedirect("/api/account"))
				.permitAll()
				.deleteCookies("JREMEMBERME")
				.and()
				.exceptionHandling().authenticationEntryPoint(new AjaxAwareAuthenticationEntryPoint(SIGNIN_URL));
	}
	
	protected void handleAjaxAuthSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException {
		
		response.setStatus(HttpServletResponse.SC_OK);
		setSecurityTokens(request, response);
		
		String returnUrl = request.getHeader("referer");
		if (StringUtils.isEmpty(returnUrl) || returnUrl.endsWith(SIGNIN_URL)) {
			returnUrl = HOME_URL;
		}
		
		mapper.writeValue(response.getOutputStream(), returnUrl);
	}
	
	protected void handleAjaxAuthFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
			throws IOException {
		
		response.sendError(SC_UNAUTHORIZED);
	}
	
	static class CsrfHeaderFilter extends OncePerRequestFilter {
		@Override
		protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
				throws ServletException, IOException {
			
			setSecurityTokens(request, response);
			filterChain.doFilter(request, response);
		}
	}
	
	static class AjaxAwareAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {
		
		AjaxAwareAuthenticationEntryPoint(String loginFormUrl) {
			super(loginFormUrl);
		}
		
		@Override
		public void commence(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException ex)
				throws IOException, ServletException {
			
			if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With"))) {
				response.sendError(403, "Forbidden");
			} else {
				super.commence(request, response, ex);
			}
		}
	}
	
	@ConfigurationProperties(prefix = "cdn.auth.admins")
	static class Admins extends LinkedHashMap<String, String> {
	}
	
	@ConfigurationProperties(prefix = "cdn.auth.guests")
	static class Guests extends LinkedHashMap<String, String> {
	}
}

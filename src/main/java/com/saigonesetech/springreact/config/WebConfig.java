package com.saigonesetech.springreact.config;

import com.saigonesetech.springreact.web.interceptor.GlobalModelInterceptor;
import com.saigonesetech.springreact.web.interceptor.PageTitleInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.script.ScriptTemplateConfigurer;
import org.springframework.web.servlet.view.script.ScriptTemplateViewResolver;

public class WebConfig implements ApplicationContextAware, WebMvcConfigurer {
	
	private ApplicationContext applicationContext;
	
	@Value(value = "${cdn.page-title}")
	private String pageTitle;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new GlobalModelInterceptor())
				.addPathPatterns("/*", "/app/**", "/api/**");
		
		registry.addInterceptor(new PageTitleInterceptor(pageTitle))
				.addPathPatterns("/**");
		
		registry.addInterceptor(expireInterceptor())
				.addPathPatterns("/**")
				.excludePathPatterns("/signin", "/expire");
	}
	
	@Bean
	public ViewResolver reactViewResolver() {
		ScriptTemplateViewResolver viewResolver = new ScriptTemplateViewResolver();
		viewResolver.setPrefix("templates/");
		viewResolver.setSuffix(".html");
		return viewResolver;
	}
	
	@Bean
	public ScriptTemplateConfigurer reactConfigurer() {
		ScriptTemplateConfigurer configurer = new ScriptTemplateConfigurer();
		configurer.setEngineName("nashorn");
		
		configurer.setScripts(SERVER_SCRIPTS);
		configurer.setRenderFunction("render");
		configurer.setSharedEngine(false);
		
		return configurer;
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry
				.addResourceHandler("/**")
				.addResourceLocations("classpath:META-INF/resources", "classpath:")
				.resourceChain(true)
				.addResolver(new ModuleResourceResolver(applicationContext));
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		// TODO Auto-generated method stub
		this.applicationContext = applicationContext;
	}
}

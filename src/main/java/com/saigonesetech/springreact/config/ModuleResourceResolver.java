package com.saigonesetech.springreact.config;

import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.resource.PathResourceResolver;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ModuleResourceResolver extends PathResourceResolver {

  private ApplicationContext applicationContext;
  private Map<String, ClassLoader> pluginClassLoaders = new HashMap<>();

  public ModuleResourceResolver(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  @Override
  protected Resource getResource(String resourcePath, Resource location) throws IOException {
    String convertedResourcePath = resourcePath;
    if (resourcePath.startsWith("admin/")) {
      convertedResourcePath = resourcePath.replaceFirst("admin/", "");
    }
    if (resourcePath.startsWith("my-projects")) {
      convertedResourcePath = resourcePath.replaceFirst("my-projects/", "");
    } else if (resourcePath.startsWith("testing-quality")) {
      convertedResourcePath = resourcePath.replaceFirst("testing-quality", "managing-quality");
    }

    String moduleKey =
        convertedResourcePath.substring(0, convertedResourcePath.indexOf(ModuleDef.PATH_SEPARATOR));
    final String resourceString =
        ((ClassPathResource) location).getPath() + ModuleDef.PATH_SEPARATOR + convertedResourcePath;

    Resource resource = new ClassPathResource(resourceString);
    if (!resource.exists()) {
      resource = getResourceFromModule(moduleKey, resourceString);
    }

    if (Objects.isNull(resource)|| !resource.exists()) {
      resource = super.getResource(resourcePath, location);
    }

    return resource;
  }

  private Resource getResourceFromModule(final String moduleKey, final String resourceString) {
    if (pluginClassLoaders.isEmpty()) {
      loadPluginClassLoaders();
    }

    if (!pluginClassLoaders.containsKey(moduleKey)) {
      return null;
    }

    String moduleResourceUrl =
        resourceString.replaceFirst(moduleKey + ModuleDef.PATH_SEPARATOR, "");
    ClassLoader classLoader = pluginClassLoaders.get(moduleKey);
    URL resourceUrl = classLoader.getResource(moduleResourceUrl);
    Resource resource = null;

    if (resourceUrl != null) {
      resource = new ClassPathResource(moduleResourceUrl, classLoader);
    }
    if (resource == null || !resource.exists()) {
      resource = new ClassPathResource(moduleResourceUrl);
    }

    return resource.exists() ? resource : null;
  }

  private void loadPluginClassLoaders() {
    applicationContext
        .getBean(PluginManager.class)
        .getPluginDefs()
        .forEach(this::loadPluginClassLoader);
  }

  private void loadPluginClassLoader(PluginDef pluginDef) {
    Class<?> pluginClass = pluginDef.getPluginClass();
    ClassLoader pluginClassLoader = pluginClass.getClassLoader();

    pluginClassLoaders.put(pluginDef.getPluginKey(), pluginClassLoader);
  }
}
